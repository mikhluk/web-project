Application "Yoda" - Simple Task Manager System.

Database dump:  ..\yoda\resources\yoda_mysql_dump_20160815.sql

Connection properties:
..\yoda\web\META-INF\context.xml
username="root"
password="root"
driverClassName="com.mysql.jdbc.Driver"
url="jdbc:mysql://localhost:3306/yoda?autoReconnect=true&amp;useSSL=false"/>

Run/Debug Configurations. Application context: /yoda

External libraries:
Tomcat 9.0.0

Application credentials:
 login   password
 - user  user
 - admin admin

Screenshots:
login page: https://gyazo.com/c64691e8caf7a570d35feb694fbc9b15
user page: https://gyazo.com/53dd687a5ab51f6d9ece9d7ec649e436
admin page: https://gyazo.com/823b06da5ccae8714274182b5369a440
