package com.epam.yoda.config.enums;

/**
 * @author Sergey Mikhluk.
 */
public enum ELocale {
    EN, RU;
}
