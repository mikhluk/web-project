package com.epam.yoda.config.enums;

/**
 * @author Sergey Mikhluk.
 */
public enum EPageAttribute {
    MESSAGE, MESSAGE_ERROR, USER, DICTIONARY_BUNDLE,
    CATEGORIES_MAP, ACTIVITIES_MAP, STATUSES_MAP, USER_TYPES_MAP, USERS_LIST,
    TASKS_LIST, CURRENT_TASK,
    CURRENT_ELEMENT
}
