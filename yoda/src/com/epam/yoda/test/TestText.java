package com.epam.yoda.test;

import com.epam.yoda.config.manager.DictionaryManager;

import static com.epam.yoda.config.enums.ELocale.RU;

/**
 * @author Sergey Mikhluk.
 */
public class TestText {
    public static void main(String[] args) {
        DictionaryManager textBundle = DictionaryManager.getInstance();
        System.out.println(textBundle.getBundleData(RU));
        System.out.println(textBundle.getBundleData(RU).get("ACTIVITY"));
    }
}
