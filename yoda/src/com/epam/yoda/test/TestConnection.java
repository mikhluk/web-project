package com.epam.yoda.test;

import com.epam.yoda.config.enums.EDataSourceType;
import com.epam.yoda.datasource.factory.DataSourceFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Enumeration;

/**
 * @author Sergey Mikhluk.
 */
public class TestConnection {
    //private static final Logger logger =  Logger.getLogger(TestConnection.class.getName());
    private static Connection getConnection() throws SQLException {
        //return ConnectionFactory.getInstance().getConnection();

//        MySQLFactory mySQLFactory = null;
//        try {
//            mySQLFactory = (MySQLFactory) ConnectionFactory.getFactory(EConnectionTypes.MYSQL);
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        MySQLConnection connection = mySQLFactory.createConnection();
//
//        MySQLConnection mySQLConnection = null;
//        try {
//            mySQLConnection = (MySQLConnection) ConnectionFactory.createConnection(EConnectionTypes.MYSQL);
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        MySQLConnection connection = mySQLConnection.createConnection();


        Connection connection = null;
        try {
            connection = DataSourceFactory.createConnection(EDataSourceType.MYSQL).getConnection();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            //logger.error(e);
        }

        return connection;
    }

    public static void main(String[] args) throws Exception {

//        DBDriverBase dbDriver = DBDriverFactory.createDriver(EDBTypes.MYSQL);
//
//        Database jdbcConn = new Database(dbDriver, "jdbc:mysql://localhost/yoda", "root", "123456");
//        jdbcConn.connect();


        testConnection();


//        try (Connection conn = getConnection(); Statement stmt = conn.createStatement()) {
//
//            ResultSet rs = stmt.executeQuery("select * from task");
//
//
//            while (rs.next()) {
//                System.out.println(
//                        rs.getLong("taskId") + " " +
//                                rs.getString("name") + " " +
//                                rs.getString("categoryId") + " " +
//                                rs.getString("activityId") + " " +
//                                rs.getString("priority"));
//
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//            //logger.error(e);
//        }
//
//        Connection conn = getConnection();
    }


    private static void traceContextNames(Context rootContext, String contextName) {
        System.out.println("Context: " + contextName);
        try {
            Context context = (Context) rootContext.lookup(contextName);
            Enumeration names = context.list("");
            while (names.hasMoreElements()) {
                System.out.println("\t" + names.nextElement());
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }


    private static void testConnection() {

        try {
            Context initialContext = new InitialContext();
            traceContextNames(initialContext, "java:module");
            traceContextNames(initialContext, "java:app");
            traceContextNames(initialContext, "java:global");
        } catch (NamingException e) {
            e.printStackTrace();
        }


    }
}

